const componentGenerateConfig = {
  description: 'Create a component',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'What is your component name?',
    },
  ],
  actions: [
    {
      type: 'add',
      path: './src/components/{{pascalCase name}}/{{pascalCase name}}.vue',
      templateFile: './tools/templates/component/component.hbs',
    },
    {
      type: 'add',
      path: './src/components/{{pascalCase name}}/{{pascalCase name}}.scss',
    },
    {
      type: 'add',
      path: './src/components/{{pascalCase name}}/{{pascalCase name}}.spec.ts',
      templateFile: './tools/templates/component/test.hbs',
    },
    {
      type: 'append',
      path: './src/components/index.ts',
      template: "export { default as {{pascalCase name}} } from './{{pascalCase name}}/{{pascalCase name}}.vue';",
    },
  ],
};

module.exports = componentGenerateConfig;
