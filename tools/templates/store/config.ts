const storeGenerateConfig = {
  description: 'Create a store module',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'What is your store module name?',
    },
  ],
  actions: [
    {
      type: 'add',
      path: './src/store/modules/{{camelCase name}}/actions.ts',
      templateFile: './tools/templates/store/actions.hbs',
    },
    {
      type: 'add',
      path: './src/store/modules/{{camelCase name}}/getters.ts',
      templateFile: './tools/templates/store/getters.hbs',
    },
    {
      type: 'add',
      path: './src/store/modules/{{camelCase name}}/index.ts',
      templateFile: './tools/templates/store/index.hbs',
    },
    {
      type: 'add',
      path: './src/store/modules/{{camelCase name}}/mutations.ts',
      templateFile: './tools/templates/store/mutations.hbs',
    },
    {
      type: 'add',
      path: './src/store/modules/{{camelCase name}}/state.ts',
      templateFile: './tools/templates/store/state.hbs',
    },
    {
      type: 'append',
      path: './src/store/modules/index.ts',
      template: 'export { default as {{camelCase name}} } from "./{{camelCase name}}";',
    },
    'É necessario importar o modulo na raiz da store',
  ],
};

module.exports = storeGenerateConfig;
