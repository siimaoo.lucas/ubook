const pageGenerateConfig = {
  description: 'Create a page',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'What is your page name?',
    },
  ],
  actions: [
    {
      type: 'add',
      path: './src/views/{{pascalCase name}}/{{pascalCase name}}.vue',
      templateFile: './tools/templates/component/component.hbs',
    },
    {
      type: 'add',
      path: './src/views/{{pascalCase name}}/{{pascalCase name}}.scss',
    },
    {
      type: 'append',
      path: './src/views/index.ts',
      template: 'export { default as {{pascalCase name}} } from "./{{pascalCase name}}";',
    },
    'É necessario criar a rota para essa página.',
  ],
};

module.exports = pageGenerateConfig;
