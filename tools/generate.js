module.exports = function (plop) {
  plop.setGenerator('component', require('./templates/component/config.ts'));
  plop.setGenerator('store', require('./templates/store/config.ts'));
  plop.setGenerator('page', require('./templates/page/config.ts'));
};
