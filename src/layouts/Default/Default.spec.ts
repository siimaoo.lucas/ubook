import { shallowMount } from '@vue/test-utils';
import Default from './Default.vue';

describe('Default.vue', () => {
  const propsData = {};

  it('should be rendered', () => {
    const search = jest.fn();
    const wrapper = shallowMount(Default, {
      propsData,
      methods: {
        search,
      },
    });
    expect(wrapper.exists()).toBe(true);
  });
});
