import { shallowMount } from '@vue/test-utils';
import Modal from './Modal.vue';

describe('Modal.vue', () => {
  let propsData = {};

  it('should be rendered', () => {
    const wrapper = shallowMount(Modal, {
      propsData,
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('should be emit event on click outside modal', () => {
    propsData = {
      value: true,
    };
    const closeModal = jest.fn();
    const wrapper = shallowMount(Modal, {
      propsData,
      methods: {
        closeModal: closeModal,
      },
    });
    wrapper.find('.overlay').trigger('click');
    expect(closeModal).toHaveBeenCalled();
  });
});
