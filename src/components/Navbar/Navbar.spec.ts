import { shallowMount } from '@vue/test-utils';
import Navbar from './Navbar.vue';

describe('Navbar.vue', () => {
  let propsData = {};

  it('should be rendered', () => {
    const wrapper = shallowMount(Navbar, {
      propsData,
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('should be render button when prop showCreateContact was passed', () => {
    propsData = {
      showCreateContact: true,
    };
    const wrapper = shallowMount(Navbar, {
      propsData,
    });
    expect(wrapper.find('[data-testid="button"]').exists()).toBe(true);
  });
});
