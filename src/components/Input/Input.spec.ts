import { shallowMount } from '@vue/test-utils';
import Input from './Input.vue';

describe('Input.vue', () => {
  let propsData = {};

  it('should be rendered', () => {
    const wrapper = shallowMount(Input, {
      propsData,
      directives: {
        mask: jest.fn(),
      },
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('should be call a function on blur', () => {
    const mockFunctionStub = jest.fn();
    const wrapper = shallowMount(Input, {
      propsData,
      listeners: {
        blur: mockFunctionStub,
      },
      directives: {
        mask: jest.fn(),
      },
    });
    const input = wrapper.find('input');
    input.trigger('blur');
    expect(mockFunctionStub).toHaveBeenCalled();
  });

  it('should be call a function on focus', () => {
    const mockFunctionStub = jest.fn();
    const wrapper = shallowMount(Input, {
      propsData,
      listeners: {
        focus: mockFunctionStub,
      },
      directives: {
        mask: jest.fn(),
      },
    });
    const input = wrapper.find('input');
    input.trigger('focus');
    expect(mockFunctionStub).toHaveBeenCalled();
  });

  it('should be call a function on input', () => {
    const mockFunctionStub = jest.fn();
    const wrapper = shallowMount(Input, {
      propsData,
      listeners: {
        input: mockFunctionStub,
      },
      directives: {
        mask: jest.fn(),
      },
    });
    const input = wrapper.find('input');
    input.trigger('input');
    expect(mockFunctionStub).toHaveBeenCalled();
  });

  it('should be show error when props error was passed', () => {
    propsData = {
      error: 'Campo obrigatorio!',
    };

    const wrapper = shallowMount(Input, {
      propsData,
      directives: {
        mask: jest.fn(),
      },
    });

    expect(wrapper.find('p').text()).toBe('Campo obrigatorio!');
  });
});
