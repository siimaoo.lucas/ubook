import { shallowMount } from '@vue/test-utils';
import Button from './Button.vue';

describe('Button.vue', () => {
  const propsData = {};

  it('should be rendered', () => {
    const wrapper = shallowMount(Button, {
      propsData,
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('should be call a function on click', () => {
    const mockFunctionStub = jest.fn();
    const wrapper = shallowMount(Button, {
      propsData,
      listeners: {
        click: mockFunctionStub,
      },
    });
    wrapper.trigger('click');
    expect(mockFunctionStub).toHaveBeenCalled();
  });
});
