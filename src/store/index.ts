import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

import { contacts } from './modules/';

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
});

const store = new Vuex.Store({
  modules: { contacts },
  plugins: [vuexLocal.plugin],
});

export const useStore = (): typeof store => store;

export default store;
