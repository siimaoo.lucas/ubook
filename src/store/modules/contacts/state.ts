import { Contact } from './types';

const state = {
  contacts: [] as Array<Contact>,
  search: '',
};

export type State = typeof state;
export default state;
