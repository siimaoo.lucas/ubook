import { State } from './state';
import { GetterTree } from 'vuex';

const getters: GetterTree<State, State> = {
  getContacts(state: State) {
    const search = state.search.toLowerCase();

    if (search) {
      return state.contacts.filter((el) => {
        return (
          el.name.toLowerCase().match(search) ||
          el.email.toLowerCase().match(search) ||
          el.phone.toLowerCase().match(search)
        );
      });
    }
    return state.contacts;
  },
  hasContacts(state: State) {
    return state.contacts.length > 0;
  },
};

export default getters;
