import { State } from './state';
import { ActionTree, ActionContext } from 'vuex';
import { Contact } from './types';

type Context = ActionContext<State, State>;

const actions: ActionTree<State, State> = {
  addContact(context: Context, payload: Pick<Contact, 'name' | 'email' | 'phone'>) {
    const id = (context.state.contacts[context.state.contacts.length - 1]?.id || 0) + 1;
    const bg = `hsla(${~~(360 * Math.random())},50%,50%,0.8)`;
    const createdAt = +new Date();

    context.commit('addContact', { ...payload, id, bg, createdAt });
  },
  editContact(context: Context, payload: Pick<Contact, 'name' | 'email' | 'phone' | 'id'>) {
    const { email, id, name, phone } = payload;
    const index = context.state.contacts.findIndex((i) => i.id === id);
    context.commit('editContact', { email, index, name, phone });
  },
  deleteContact(context: Context, payload: number) {
    const index = context.state.contacts.findIndex((i) => i.id === payload);
    context.commit('deleteContact', index);
  },
  search(context: Context, payload: string) {
    context.commit('changeSearch', payload);
  },
};

export default actions;
