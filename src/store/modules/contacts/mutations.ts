import { State } from './state';
import { MutationTree } from 'vuex';
import { Contact } from './types';

const mutations: MutationTree<State> = {
  addContact(state: State, payload: Contact) {
    state.contacts.push({
      ...payload,
    });
  },
  editContact(state: State, payload: Pick<Contact, 'name' | 'email' | 'phone'> & { index: number }) {
    const { name, email, phone, index } = payload;
    const contacts = [...state.contacts];
    contacts[index] = { ...state.contacts[index], name, email, phone };
    state.contacts = contacts;
  },
  deleteContact(state: State, payload: number) {
    const contacts = [...state.contacts];
    contacts.splice(payload, 1);
    state.contacts = contacts;
  },
  changeSearch(state: State, payload: string) {
    state.search = payload;
  },
};

export default mutations;
