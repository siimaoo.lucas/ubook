export type Contact = {
  name: string;
  email: string;
  phone: string;
  createdAt: number;
  bg: string;
  id: number;
};
