import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VueCompositionAPI from '@vue/composition-api';
import VueHead from 'vue-head';
import VueMask from 'v-mask';

Vue.config.productionTip = false;

Vue.use(VueCompositionAPI);
Vue.use(VueHead);
Vue.use(VueMask);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
